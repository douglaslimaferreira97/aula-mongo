var express = require('express');
var Cliente = require('../models/cliente');
var router = express.Router();

router.get('/', (req, res)=>{
    Cliente.getAll(res);
});
router.put('/:id', (req, res)=>{
    Cliente.update(req, res);
});
router.get('/:id', (req, res)=>{
    Cliente.getById(req.params.id, res);
});
router.post('/', (req, res)=>{
    Cliente.create(req, res);
});
router.delete('/:id', (req, res)=>{
    Cliente.delete(req.params._id, res);
});
module.exports = router;