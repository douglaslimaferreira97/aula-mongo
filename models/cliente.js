var Mongoose = require('./index').mongoose;

var ClienteSchema = new Mongoose.Schema({
    nome: {type: String, required: true, unique: true},
    cpf: {type: String,required: true, unique: true},
    cidade: {type: String,required: true, unique: true}
}, {collection: 'Cliente'});

var Cliente = Mongoose.model('Cliente', ClienteSchema);
var crud = {
    create:(req, res)=>{
       var cli = new Cliente({
           nome: req.body.nome,
           cpf: req.body.cpf,
           cidade: req.body.cidade
       });
       cli.save((error, cliente)=>{
           if(error) res.json(error);
           res.json(cliente);
       });
    },
    getAll:(res)=>{
        Cliente.find({}, (error, cliente)=>{
            if(error) res.json(error);
            res.json(cliente);
        });
    },
    getById: (id, res)=>{
        Cliente.findById(id, (error, cliente)=>{
            if(error) res.json(error);
            res.json(cliente);
        });
    },
    update: (req, res)=>{
        var cliente = {};
        cliente.nome = req.body.nome;
        cliente.cpf = req.body.cpf;
        cliente.cidade = req.body.cidade;        
        Cliente.update({id: req.params._id}, {
            nome: req.body.nome,
            cpf: req.body.cpf,
            cidade: req.body.cidade
        }, (error, cli)=>{
            if(error) res.json(error);
            res.json(cli);
        });
    },
    delete: (id, res)=>{
        Cliente.deleteOne({id: id}, (error, cliente)=>{
            if(error) res.json(error);
            res.json(cliente);
        });
    }
}
module.exports = crud;